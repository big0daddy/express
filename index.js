const Discord = require('discord.js');
const { prefix, token } = require('./config.json');
const client = new Discord.Client();




// Here we load the config.json file that contains our token and our prefix values. 
const config = require("./config.json");
// config.token contains the bot's token
// config.prefix contains the message prefix.

client.on("ready", () => {
  // This event will run if the bot starts, and logs in, successfully.
  console.log(`Bot has started, with ${client.users.size} users, in ${client.channels.size} channels of ${client.guilds.size} guilds.`); 
  // Example of changing the bot's playing game to something useful. `client.user` is what the
  // docs refer to as the "ClientUser".\
  client.user.setActivity("Do ..help for commands")
 
});

client.on("guildCreate", guild => {
  // This event triggers when the bot joins a guild.
  console.log(`New guild joined: ${guild.name} (id: ${guild.id}). This server has ${guild.memberCount} members!`);
  client.user.setActivity(`Serving ${client.guilds.size} servers`);
});

client.on("guildDelete", guild => {
  // this event triggers when the bot is removed from a guild.
  console.log(`I have been removed from: ${guild.name} (id: ${guild.id})`);
  client.user.setActivity(`Serving ${client.guilds.size} servers`);
});


client.on("message", async message => {
  // This event will run on every single message received, from any channel or DM.
  
  // It's good practice to ignore other bots. This also makes your bot ignore itself
  // and not get into a spam loop (we call that "botception").
  if(message.author.bot) return;
  
  // Also good practice to ignore any message that does not start with our prefix, 
  // which is set in the configuration file.
  if(message.content.indexOf(config.prefix) !== 0) return;
  
  // Here we separate our "command" name, and our "arguments" for the command. 
  // e.g. if we have the message "+say Is this the real life?" , we'll get the following:
  // command = say
  // args = ["Is", "this", "the", "real", "life?"]
  const args = message.content.slice(config.prefix.length).trim().split(/ +/g);
  const command = args.shift().toLowerCase();
  
  // Let's go with a few common example commands! Feel free to delete or change those.
  
  if(command === "ping") {
    // Calculates ping between sending a message and editing it, giving a nice round-trip latency.
    // The second ping is an average latency between the bot and the websocket server (one-way, not round-trip)
    const m = await message.channel.send("Ping?");
    m.edit(`Pong! Latency is ${m.createdTimestamp - message.createdTimestamp}ms. API Latency is ${Math.round(client.ping)}ms`);
  }
  
  if(command === "say") {
    // makes the bot say something and delete the message. As an example, it's open to anyone to use. 
    // To get the "message" itself we join the `args` back into a string with spaces: 
    const sayMessage = args.join(" ");
    // Then we delete the command message (sneaky, right?). The catch just ignores the error with a cute smiley thing.
    message.delete().catch(O_o=>{}); 
    // And we get the bot to say the thing: 
    message.channel.send(sayMessage);
  }
  
  if(command === "kick") {
    // This command must be limited to mods and admins. In this example we just hardcode the role names.
    // Please read on Array.some() to understand this bit: 
    // https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Array/some?
    if(!message.member.roles.some(r=>["Owner", "Moderator"].includes(r.name)) )
      return message.reply("Sorry, you don't have permissions to use this!");
    
    // Let's first check if we have a member and if we can kick them!
    // message.mentions.members is a collection of people that have been mentioned, as GuildMembers.
    // We can also support getting the member by ID, which would be args[0]
    let member = message.mentions.members.first() || message.guild.members.get(args[0]);
    if(!member)
      return message.reply("Please mention a valid member of this server");
    if(!member.kickable) 
      return message.reply("I cannot kick this user! Do they have a higher role? Do I have kick permissions?");
    
    // slice(1) removes the first part, which here should be the user mention or ID
    // join(' ') takes all the various parts to make it a single string.
    let reason = args.slice(1).join(' ');
    if(!reason) reason = "No reason provided";
    
    // Now, time for a swift kick in the nuts!
    await member.kick(reason)
      .catch(error => message.reply(`Sorry ${message.author} I couldn't kick because of : ${error}`));
    message.reply(`**${member.user.tag} has been kicked by ${message.author.tag} because: ${reason}**`);

  }
  
  if(command === "ban") {
    // Most of this command is identical to kick, except that here we'll only let admins do it.
    // In the real world mods could ban too, but this is just an example, right? ;)
    if(!message.member.roles.some(r=>["Owner"].includes(r.name)) )
      return message.reply("Sorry, you don't have permissions to use this!");
    
    let member = message.mentions.members.first();
    if(!member)
      return message.reply("Please mention a valid member of this server");
    if(!member.bannable) 
      return message.reply("I cannot ban this user! Do they have a higher role? Do I have ban permissions?");

    let reason = args.slice(1).join(' ');
    if(!reason) reason = "No reason provided";
    
    await member.ban(reason)
      .catch(error => message.reply(`Sorry ${message.author} I couldn't ban because of : ${error}`));
    message.reply(`**${member.user.tag} has been banned by ${message.author.tag} because: ${reason}**`);
  }
  
  if(command === "purge") {
    // This command removes all messages from all users in the channel, up to 100.
    
    // get the delete count, as an actual number.
    const deleteCount = parseInt(args[0], 10);
    
    // Ooooh nice, combined conditions. <3
    if(!deleteCount || deleteCount < 2 || deleteCount > 100)
      return message.reply("Please provide a number between 2 and 100 for the number of messages to delete");
    
    // So we get our messages, and delete them. Simple enough, right?
    const fetched = await message.channel.fetchMessages({limit: deleteCount});
    message.channel.bulkDelete(fetched)
      .catch(error => message.reply(`Couldn't delete messages because of: ${error}`));
  }
});
client.on("message", (message) => {
  if (message.content.startsWith("hi")) {
    message.author.send("Hi There");
  }
});
client.on("message", (message) => {
  if (message.content.startsWith("hru")) {
    message.author.send("I'm Good you");
  }
});
client.on("message", (message) => {
  if (message.content.startsWith("im ok")) {
    message.channel.send("that's good");
  }
});
client.on("message", (message) => {
  if (message.content.startsWith("33")) {
    message.delete().catch(O_o=>{}); 
    message.channel.send("**Kitty is the best <3**");
    message.channel.send("**and so is Daddy**");
  }
});
client.on("message", (message) => {
  if (message.content.startsWith("..help")) {
    message.channel.send({embed: {
      color: 3447003,
      author: {
        icon_url: client.user.avatarURL
      },
      title: "Help Commands",
      fields: [{
        name: "Ban",
        value: "To ban a user do **..ban {user}**"
      },
      {
        name: "Kick",
        value: "To kick a user do **..kick {user}**"
      },
      {
        name: "Prune",
        value: "To prune or delete messages do **..prune {amount}**"
      },
      {
        name: "Say",
        value: "To speak with bot use the command **..say {message}.**"
      },
      {
        name: "Server Info",
        value: "To find out server info do command **..Server**"
      }
  ],
  timestamp: new Date(),
  footer: {
    icon_url: client.user.avatarURL,
    text: "Daddy Bot"
  }
}
});
  // makes the bot say something and delete the message. As an example, it's open to anyone to use. 
  // To get the "message" itself we join the `args` back into a string with spaces: 
  // Then we delete the command message (sneaky, right?). The catch just ignores the error with a cute smiley thing.
  // And we get the bot to say the thing: 
  }
});
client.on("message", (message) => {
  if (message.content.startsWith("BIG GAY")) {
    message.author.send("YES BIG GAY HERE");
  }
});
client.on("message", (message) => {
  if (message.content.startsWith("..server")) {
    message.channel.send({embed: {
      color: 9257003,
      author: {
        icon_url:"https://cdn.discordapp.com/attachments/511077824518553603/512336729420857344/RevengeLove44.jpg"
      },
      thumbnail: {
        url: "https://cdn.discordapp.com/attachments/511077824518553603/512336729420857344/RevengeLove44.jpg"
      },

      image: {
        url: "https://cdn.discordapp.com/attachments/511077824518553603/512349445359271962/ctbjHIamoLfDFm4r-croppedasw02.jpg"
      },
      
      title: "Server Info",
      fields: [{
        name: "Member Count",
        value: `**${message.guild.memberCount} **`
      },
      {
        name: "Bot Count",
        value: `**${message.guild.members.filter(member => member.user.bot).size}**`
      },
      {
        name: "Roles",
        value: `**${message.channel.guild.roles.size}**`
      },
      {
        name: "Channels",
        value: `**${message.guild.channels.size.toLocaleString()}**`
      },
      {
        name: "Usage",
        value: `${(process.memoryUsage().heapUsed / 1024 / 1024).toFixed(2)} MB`
      },
      {
        name: "Ping",
        value: `**${Math.round(client.ping)}ms**`
      },
      {
        name: "Guilds",
        value: `**${client.guilds.size}**`
      },
      {
        name: "Owner",
        value: `${message.guild.owner}`
      },
      {
        name: "Region",
        value: `${message.guild.region}`
      }
     
    ],
  timestamp: new Date(),
  footer: {
    icon_url: client.user.avatarURL,
    text: `${message.guild.name} ${message.guild.id}`
    
  }
}
});
  // makes the bot say something and delete the message. As an example, it's open to anyone to use. 
  // To get the "message" itself we join the `args` back into a string with spaces: 
  // Then we delete the command message (sneaky, right?). The catch just ignores the error with a cute smiley thing.
  // And we get the bot to say the thing: 
  }
});
  



    
  client.login('NTEwNTAyNjc4Mjc1NTU1MzQz.DsgeVg.QsUiklorNFNou3E06f-todC5CCg');